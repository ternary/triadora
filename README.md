# triadora

Macro-assembler for ternary processor TRIADOR

See https://hackaday.io/project/28579-homebrew-ternary-computer

Emulator is based on https://github.com/ssloy/triador
